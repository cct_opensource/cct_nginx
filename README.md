
TM-1 Nginx Front-End
====================

We use nginx on the front end for the TM-1. This is a production level open source web server
that allows us handle multiple domains on a single server.

/etc/letsencrypt/live/awsdev6.electricalscience.com/
cat /var/log/nginx/access.log

[Nginx Documentation](docs/nginx.md)

Installation
------------

	$ sudo apt-get update
	$ sudo apt-get install nginx

TM-1 Nginx Configuration
------------------------

The way we currently have this project setup is to install a nginx configuration file (/etc/nginx/conf.d/tm1web.conf) inside the image, this file is stored on the host and shared into the docker container.

This configuration file is designed to forward all traffic from port 443 (SSL port) to port 3001 where we run the tm1_web docker image.  All traffic to port 80 is redirected to port 443 using a 301 redirect.

