# Use an official nginx runtime as a base image
FROM nginx

MAINTAINER Patrick Farrell <patrick@creativecollisionstech.com>
LABEL Description="Nginx image for CCT Base Web System" Vendor="creativecollisionstech" Version="0.9.0"

# Create the directory where we will store and access the security files.
RUN mkdir /security
