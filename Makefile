
# Makefile for the cct_nginx Front-end Nginx Web Server.

# Copyright 2019 Creative Collisions Tech, LLC  All rights reserved.

.PHONY: build

DOCKER_IMAGE_NAME_NGINX ?= docker.creativecollisionstech.com/cct/cct_nginx

cct_CONFIG_FOLDER=/secureapp/cct/etc/
COMPOSE_YML_FILE=cct-nginx-compose.yml

# Host Configuration
HOST_HTTP_PORT ?= 80
HOST_HTTPS_PORT ?= 443
HOST_NGINX_CONFIG_FILE_PATH=/secureapp/cct/etc/nginx/conf.d
HOST_NGINX_LOG_FOLDER=/secureapp/mnt/logs/nginx
HOST_SSL_CERT=/etc/letsencrypt/live/<domain>/fullchain.pem
HOST_SSL_PRIV_KEY=/etc/letsencrypt/live/<domain>/privkey.pem


# Image Configuration
IMAGE_HTTP_PORT ?= 80
IMAGE_HTTPS_PORT ?= 443
IMAGE_NGINX_CONFIG_FILE_PATH ?= /etc/nginx/conf.d
IMAGE_NGINX_LOG_FOLDER=/var/log/nginx
IMAGE_SSL_CERT=/security/fullchain.pem
IMAGE_SSL_PRIV_KEY=/security/privkey.pem

INITDIR=/etc/init.d

build: build_nginx

install:
	# Make the directory for the nginx log folder
	mkdir -p ${HOST_NGINX_LOG_FOLDER}
	# Make the directory for the Nginx Config file
	mkdir -p ${HOST_NGINX_CONFIG_FILE_PATH}
	# Install the TM-1 Startup Script
	cp cct-nginx-compose.yml ${cct_CONFIG_FOLDER}/
	cp etc/nginx/conf.d/cctweb.conf ${HOST_NGINX_CONFIG_FILE_PATH}
	cp -p etc/init.d/cctnginx ${INITDIR}
	update-rc.d cctnginx defaults
	chmod 777 ${INITDIR}/cctnginx


uninstall:
	# Remove the TM-1 Startup Script
	rm -f ${INITDIR}/cctnginx
	update-rc.d cctnginx remove
	rm ${HOST_NGINX_CONFIG_FILE_PATH}
	rm ${COMPOSE_YML_FILE}

build_nginx:
	docker build -f Dockerfile -t ${DOCKER_IMAGE_NAME_NGINX} .

clean:
	docker rmi ${DOCKER_IMAGE_NAME_NGINX}

push:
	docker push ${DOCKER_IMAGE_NAME_NGINX}

pull:
	docker pull ${DOCKER_IMAGE_NAME_NGINX}

image-run:
	docker run \
	-v ${HOST_SSL_CERT}:${IMAGE_SSL_CERT} \
	-v ${HOST_SSL_PRIV_KEY}:${IMAGE_SSL_PRIV_KEY} \
	-v ${HOST_NGINX_CONFIG_FILE_PATH}:${IMAGE_NGINX_CONFIG_FILE_PATH} \
	-v ${HOST_NGINX_LOG_FOLDER}:${IMAGE_NGINX_LOG_FOLDER} \
	-p ${HOST_HTTP_PORT}:${IMAGE_HTTP_PORT} \
	-p ${HOST_HTTPS_PORT}:${IMAGE_HTTPS_PORT} \
	$(DOCKER_IMAGE_NAME_NGINX)

dev:
	docker-compose -f cct-nginx-compose.yml up --force-recreate

dev-d:
	docker-compose -f cct-nginx-compose.yml up --force-recreate -d
